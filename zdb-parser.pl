#!/usr/bin/perl
#
#----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <mateusz@serveraptor.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return.
#----------------------------------------------------------------------------
#
# To use this script you will need dump from ZFS metadata. You can generate
# that dump using zdb(1) command:
#   zdb -ddddd -e data/set/name > data-set-name
# After that you can feed script with this data:
#   cat data-set-name | ./zdb-parser.pl
#

use strict;
use warnings;

use Data::Dumper;
use File::Path qw(make_path);
use File::Basename;

# Settings:
#
my $debug = 0;
my $restore_path = "/tank/home/";
my $dataset_name = "data/home";

# Code
my @blk;
my $cnt = 0;
my $parse_iblocks = 0;
my $iblock = 0;
my $parse = 0;

sub debug($) {
  my $arg = shift;
  print STDERR "[DEBUG] $arg" if $debug eq 1;
}

while(<>) {
  debug("Parsing line: $_");
  # start of object
  if(/Object/) {
    $parse = 0;
    debug("new object (count: $cnt)\n");
  }

  if(/ZFS plain file/) {
    debug("object type is regular file (count: $cnt\n");
    $parse = 1;
    next;
  }

  if($parse == 1 && /path/) {
    my @tpath = split(' ', $_, 2);
    $blk[$cnt]{'path'} = $tpath[1];
    chomp $blk[$cnt]{'path'};
    debug("found path: $tpath[1]\n");
  }
  if($parse == 1 && /\tsize/) {
    my @tsize = split(' ', $_);
    $blk[$cnt]{'size'} = $tsize[1];
  }
  
  if($parse == 1 && /mode/) {
    my @tmode = split(' ', $_);
    $blk[$cnt]{'mode'} = $tmode[1];
  }

  if(/Indirect blocks:/ && $parse == 1) { #start gathering indirect blocks 
    debug("found list of indirect blocks");
    $parse_iblocks = 1;
    next;
  }

  if($parse_iblocks == 1 && $parse == 1) {  
    if(!/segment \[[0-9]+, [0-9]+\) size.*/ && /L([0-9])/) {
      chomp;
      debug("found indirect block entry (count: $iblock): $_\n ");
      $blk[$cnt]{'iblocks'}[$iblock] = $_;
      $iblock++;
    } else {
      debug("end of parsing indirect blocks\n");
      $parse_iblocks = 0;
      $iblock = 0;
      $cnt++;
    }
  }
}
print "Finished. I found $cnt files. Starting recovery... \n\n";

foreach (@blk) {
  my %file = %$_;
  my $full_path = "${restore_path}/${file{'path'}}";
  my $dirname = dirname($full_path);
  if($full_path !~ /<xattrdir>/) {
    print "Recovering file: path: \"$file{'path'}\", size: $file{'size'}, mode: $file{'mode'}\n";
    if(! -d $dirname)  {
      if( -f $dirname) {
        die "$dirname is file but it should be directory";
      } else {
        make_path($dirname);
      }
    }
    if (! -e $full_path) {
      foreach my $iblock (@{$file{'iblocks'}}) {
       my @tblock = split(' ', $iblock);
        if ($tblock[1] =~ /L(0|1])/) {
          system("zdb -e -R $dataset_name ${tblock[2]}:r 2>/dev/null| dd bs=128K >> \"$full_path\" 2>/dev/null");
        }
      }
    } else {
      print("File $full_path exists - skipping.\n");
    } 
  }
}
